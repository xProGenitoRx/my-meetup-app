import React from "react";
import { Routes, Route } from "react-router-dom";

import AllMeetupsPage from "./page/AllMeetups";
import NewMeetupsPage from "./page/NewMeetup";
import FavoritesPage from "./page/Favorites";
import Layout from "./component/layouts/Layout";

function App() {
  // localhost:3000/

  return (
    <div>
      <Layout>
        <Routes>
          <Route path="/" element={<AllMeetupsPage />} />
          <Route path="/new-meetup" element={<NewMeetupsPage />} />
          <Route path="/favorites" element={<FavoritesPage />} />
        </Routes>
      </Layout>
    </div>
  );
}

export default App;
