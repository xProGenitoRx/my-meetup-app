import { useNavigate } from 'react-router-dom';
import NewMeetupForm from "../component/meetups/NewMeetupForm";

export default function NewMeetupsPage() {
    const navigate = useNavigate();

  function addMeetupHandler(meetupData) {
      fetch(
          'https://my-meetup-app-5e320-default-rtdb.europe-west1.firebasedatabase.app/meetups.json',
          {
              method: 'POST',
              body: JSON.stringify(meetupData),
              headers: {
                  'Content-Type': 'application/json'
              }
          }
        ).then(_=> navigate('/'));
  }
  return (
    <section>
      <h1>Add New Meetup</h1>
      <NewMeetupForm onAddMeetup={addMeetupHandler} />
    </section>
  );
}
